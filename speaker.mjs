import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {
        return(`"${super.say(phrase)}" very confidently.`)
    }

    drinkWater(volume){
        return (`${this.name} drinks ${volume} glasses of water`)
    }
}

let john = new Person('John');
console.log(john.say('Hello!'));

let bob = new Speaker('Bob');
console.log(bob.say('Hi!'));
console.log(bob.drinkWater('2'));

